package com.thoughtworks.springbootemployee;

public class Employee {
    private  Long id;
    private  String name;
    private  Integer age;
    private  String gender;
    private  Integer salary;

    private Long comapnyId;

    public Employee(Long id, String name, Integer age, String gender, Integer salary,Long comapnyId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
        this.comapnyId = comapnyId;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getComapnyId() {
        return comapnyId;
    }

    public void setComapnyId(Long comapnyId) {
        this.comapnyId = comapnyId;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
