package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private List<Employee> employees;

    public EmployeeRepository() {
        this.employees = new ArrayList<>();
        employees.add(new Employee(1L, "Lucy", 20, "female", 2000,1L));
        employees.add(new Employee(2L, "Lily", 20, "female", 2000,1L));
        employees.add(new Employee(3L, "Tom", 19, "male", 2000,2L));
        employees.add(new Employee(4L, "Ivan", 18, "male", 2000,2L));
    }
    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> findAllWithGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee insert(Employee employee) {
        employee.setId(generateID());
        employees.add(employee);
        return employee;
    }

    private Long generateID() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Employee> geEmployeeWithPage(Integer page, Integer size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Employee updateEmployee(long id, Employee employee) {
        Employee updateEmployee = findById(id);
        if(employee.getAge() != null)
            updateEmployee.setAge(employee.getAge());

        if(employee.getGender() !=null)
            updateEmployee.setGender(employee.getGender());

        if(employee.getName() !=null)
            updateEmployee.setName(employee.getName());

        if(employee.getSalary() != null)
            updateEmployee.setSalary(employee.getSalary());
        return updateEmployee;
    }


    public void deleteEmployee(long id) {
        Employee deleteEmployee = findById(id);
        if (deleteEmployee != null)
            employees.remove(deleteEmployee);
    }
}
