package com.thoughtworks.springbootemployee.repository;
import com.thoughtworks.springbootemployee.Company;
import com.thoughtworks.springbootemployee.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CompanyRespository {
    private final List<Company> companyList;
    @Autowired
    private EmployeeRepository employeeRepository;
    public CompanyRespository(List<Company> companyList){
        this.companyList = companyList;
        companyList.add(new Company(1L,"spring"));
        companyList.add(new Company(2L,"boot"));
        companyList.add(new Company(101L,"start"));
        companyList.add(new Company(4L,"work"));
    }


    public List<Company> getAllCompany() {
        return companyList;
    }

    public Company getById(long id) {
        return companyList.stream()
                .filter(company -> company.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getEmployeeById(long id) {
        return employeeRepository.findAll().stream().
                filter(employee -> Objects.equals(employee.getComapnyId(),id)).
                collect(Collectors.toList());
    }

    public List<Company> getCompanyByPage(int page, int size) {
        return companyList.stream().skip((page-1)*size).limit(size).collect(Collectors.toList());
    }


    public Company addCompany(Company company) {
        company.setId(generateId());
        companyList.add(company);
        return company;
    }

    private long generateId() {
        return companyList.stream().max(Comparator.comparing(Company::getId)).
                map(company -> company.getId()+1).orElse(1L);
    }

    public Company updateCompany(Long id,Company company) {
        Company updateCompany = getById(id);
        if(company.getName() != null){
            updateCompany.setName(company.getName());
        }
        return updateCompany;
    }


    public void deleteCompany(Long id) {
        Company deleteCompany = getById(id);
        if (deleteCompany != null)
            companyList.remove(deleteCompany);
    }
}
