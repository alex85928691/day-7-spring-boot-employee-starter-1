package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.Company;
import com.thoughtworks.springbootemployee.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRespository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    private final CompanyRespository companyRespository;

    public CompanyController(CompanyRespository companyRespository) {
        this.companyRespository = companyRespository;
    }

    @GetMapping()
    public List<Company> getAllCompany(){
        return companyRespository.getAllCompany();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id){
        return companyRespository.getById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeeByCompanyId(@PathVariable Long id){
        return companyRespository.getEmployeeById(id);
    }

    @GetMapping(params = {"page","size"})
    public List<Company> getCompanyByPage(@RequestParam int page , @RequestParam int size){
      return companyRespository.getCompanyByPage(page,size);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company){
        return companyRespository.addCompany(company);
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable Long id, @RequestBody Company company){
        return companyRespository.updateCompany(id,company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id){
        companyRespository.deleteCompany(id);

    }
}
